<?php

/**
 * Code source de la classe LibricielPasswordTest.
 *
 * PHP 5.3
 *
 * @package LibricielPassword
 * @subpackage Test.Case.Utility
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */
App::uses('LibricielPassword', 'LibricielPassword.Utility');

/**
 * La classe LibricielPasswordTest effectue les tests unitaires de
 * la classe LibricielPassword.
 *
 * @package LibricielPassword
 * @subpackage Test.Case.Utility
 */
class LibricielPasswordTest extends CakeTestCase
{

    public function setUp()
    {
        parent::setUp();

        Configure::write('LibricielPassword.minStrength', null);
    }

    /**
     * Test de la méthode LibricielPassword::minStrength() sans configuration.
     *
     * @covers LibricielPassword::minStrength
     */
    public function testMinStrengthWithoutConfiguration()
    {
        $this->assertEquals(4, LibricielPassword::minStrength());
    }

    /**
     * Test de la méthode LibricielPassword::minStrength() avec configuration
     * correcte.
     *
     * @covers LibricielPassword::minStrength
     */
    public function testMinStrengthWithCorrectConfiguration()
    {
        Configure::write('LibricielPassword.minStrength', 5);
        $this->assertEquals(5, LibricielPassword::minStrength());
    }

    /**
     * Test de la méthode LibricielPassword::minStrength() avec configuration
     * incorrecte et émission d'une notice.
     * 
     * @expectedException PHPUnit_Framework_Error_Notice
     * @expectedExceptionMessage LibricielPassword::minStrength, invalid configured strength "foo" for "LibricielPassword.minStrength" key.
     * @expectedExceptionCode E_USER_NOTICE
     *
     * @covers LibricielPassword::minStrength
     */
    public function testMinStrengthWithIncorrectConfigurationAndNotice()
    {
        Configure::write('LibricielPassword.minStrength', 'foo');
        LibricielPassword::minStrength();
    }

    /**
     * Test de la méthode LibricielPassword::minStrength() avec configuration
     * incorrecte et sans l'émission de la notice.
     * 
     * @covers LibricielPassword::minStrength
     */
    public function testMinStrengthWithIncorrectConfigurationAndWithoutNotice()
    {
        Configure::write('LibricielPassword.minStrength', 'foo');
        $this->assertEquals(4, @LibricielPassword::minStrength());
    }
}

?>
