<?php
/**
 * Code source de la classe LibricielPasswordBehaviorTest.
 *
 * PHP 5.3
 *
 * @package LibricielPassword
 * @subpackage Test.Case.Model.Behavior
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */
App::uses('AppModel', 'Model');

/**
 * La classe LibricielPasswordBehaviorTest effectue les tests unitaires de
 * la classe LibricielPasswordBehavior.
 *
 * @package LibricielPassword
 * @subpackage Test.Case.Model.Behavior
 */
class LibricielPasswordBehaviorTest extends CakeTestCase
{

    /**
     * Fixtures utilisées par cette classe de tests unitaires.
     *
     * @var array
     */
    public $fixtures = [
        'core.Site'
    ];

    /**
     * Méthode exécutée avant chaque méthode de test.
     */
    public function setUp()
    {
        parent::setUp();

        ClassRegistry::flush();
        Cache::clear();

        Configure::write('Config.language', 'eng');
        Configure::write('LibricielPassword.minStrength', 3);

        $this->Site = ClassRegistry::init('Site');
        $this->Site->validate = [];
    }

    /**
     * Méthode exécutée après chaque méthode de test.
     */
    public function tearDown()
    {
        unset($this->Site);
        parent::tearDown();
    }

    /**
     * Test de la méthode LibricielPasswordBehavior::compareWith() du plugin
     * LibricielPassword.
     *
     * @covers LibricielPasswordBehavior::compareWith
     */
    public function testCompareWith()
    {
        $this->Site->Behaviors->attach('LibricielPassword.LibricielPassword');
        $this->Site->data = [
            $this->Site->alias => [
                'field_1' => 'my password',
                'field_2' => 'not my password'
            ]
        ];

        $this->assertFalse($this->Site->compareWith(null, null));
        $this->assertTrue($this->Site->compareWith(['my password'], 'field_1'));
        $this->assertFalse($this->Site->compareWith(['my password'], 'field_2'));
    }

    /**
     * Test de la méthode LibricielPasswordBehavior::checkPasswordStrength() du
     * plugin LibricielPassword.
     *
     * @covers LibricielPasswordBehavior::checkPasswordStrength
     */
    public function testCheckPasswordStrength()
    {
        $this->Site->Behaviors->attach('LibricielPassword.LibricielPassword');
        $this->assertFalse($this->Site->checkPasswordStrength(['field_1' => null]));
        $this->assertTrue($this->Site->checkPasswordStrength(['field_1' => '4A42591CB97BF56FBC93']));
    }

    /**
     * Test de la méthode LibricielPasswordBehavior::afterValidate() du plugin
     * LibricielPassword.
     *
     * @covers LibricielPasswordBehavior::afterValidate
     */
    public function testAfterValidate()
    {
        $this->Site->Behaviors->attach('LibricielPassword.LibricielPassword');
        $this->Site->validate = [
            'field_1' => [
                'notBlank' => [
                    'allowEmpty' => false,
                    'required' => null,
                    'rule' => ['notBlank'],
                    'message' => false
                ],
                'checkPasswordStrength' => [
                    'allowEmpty' => true,
                    'required' => false,
                    'rule' => ['checkPasswordStrength'],
                    'message' => false
                ]
            ]
        ];

        $data = [
            $this->Site->alias => [
                'field_1' => 'my password'
            ]
        ];

        $this->Site->create($data);
        $this->assertFalse($this->Site->validates());

        $expected = [
            'field_1' => [
                'Veuillez renseigner un mot de passe de force 3 au minimum'
            ]
        ];
        $this->assertEquals($expected, $this->Site->validationErrors);
    }
    
}
