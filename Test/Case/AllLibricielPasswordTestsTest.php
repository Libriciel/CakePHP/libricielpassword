<?php

/**
 * AllLibricielPasswordTests file
 *
 * PHP 5.3
 *
 * @package LibricielPassword
 * @subpackage Test.Case
 */

/**
 * AllLibricielPasswordTests class
 *
 * This test group will run all tests.
 *
 * @package LibricielPassword
 * @subpackage Test.Case
 */
class AllLibricielPasswordTests extends PHPUnit_Framework_TestSuite
{

    /**
     * Test suite with all test case files.
     */
    public static function suite()
    {
        $suite = new CakeTestSuite('All LibricielPassword tests');
        $suite->addTestDirectoryRecursive(dirname(__FILE__) . DS . '..' . DS . 'Case' . DS);
        return $suite;
    }

}
