<?php
use Libriciel\Utility\Password\PasswordStrengthMeterAnssi;

App::uses('LibricielPassword', 'LibricielPassword.Utility');

class LibricielPasswordBehavior extends ModelBehavior
{
    public function checkPasswordStrength(Model $Model, array $check = [])
    {
        $strength = LibricielPassword::minStrength();

        $success = true;
        foreach ($check as $field => $value) {
            if (PasswordStrengthMeterAnssi::strength($value) < $strength) {
                $success = false;
                $message = 'Veuillez renseigner un mot de passe de force %d au minimum';
                $Model->invalidate($field, sprintf($message, $strength));
            }
        }
        return $success;
    }

    /**
     * Compare la valeur du champ avec la valeur d'un champ de référence.
     *
     * @param Model $Model Le modèle utilisant ce behavior
     * @param array $check La liste de valeurs à vérifier
     * @param string $fieldName Le nom du champ avec lequel comparer les valeurs
     * @return boolean
     */
    public function compareWith(Model $Model, $check = [], $fieldName)
    {
        if (is_array($check) === false) {
            return false;
        }

        foreach ($check as $key => $value) {
            $reference = Hash::get($Model->data, "{$Model->alias}.{$fieldName}");
            if ($value != $reference) {
                return false;
            } else {
                continue;
            }
        }

        return true;
    }

    /**
     * Suppression des erreurs de validation non traduites (Validate::checkPasswordStrength)
     * lorsqu'il existe au moins un autre message d'erreur pour le champ.
     */
    public function afterValidate(Model $Model)
    {
        $result = parent::afterValidate($Model);

        foreach ($Model->validationErrors as $field => $errors) {
            $idx = array_search('Validate::checkPasswordStrength', $errors, true);
            if ($idx !== false && $idx > 0) {
                unset($Model->validationErrors[$field][$idx]);
            }
        }

        return $result;
    }
}
