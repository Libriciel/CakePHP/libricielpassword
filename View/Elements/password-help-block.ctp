<?php
App::uses('LibricielPassword', 'LibricielPassword.Utility');
$minStrength = isset($minStrength) ? $minStrength : LibricielPassword::minStrength();
?>
<small>Mot de passe de force <?php echo $minStrength;?> minimum.
<button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal-password-help" title="En savoir plus à propos du calcul de la force d'un mot de passe">
    <span class="fa fa-question fa-sm"> </span>
</button></small>
