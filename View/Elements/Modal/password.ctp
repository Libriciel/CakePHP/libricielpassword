<?php
use Libriciel\Utility\Password\PasswordGeneratorAnssi;

App::uses('LibricielPassword', 'LibricielPassword.Utility');

$config = PasswordGeneratorAnssi::config();
$false = array_fill_keys(array_keys($config), false);
$minStrength = isset($minStrength) ? $minStrength : LibricielPassword::minStrength();
?>

<div class="modal fade" id="modal-password-help" tabindex="-1" role="dialog" aria-labelledby="modal-password-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-password-label">Calcul de la force d'un mot de passe</h4>
            </div>

            <div class="modal-body">
                <p>La force du mot de passe indique la difficulté à deviner le mot de passe. Celle-ci dépend du nombre de caractères utilisés et du nombre de caractères disponibles dans la classe de caractères choisie, soit du nombre de combinaisons possibles.</p>
                
                <div class="h4">Exemples</div>

                <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-striped libriciel-password libriciel-password-examples">
                        <thead>
                            <tr>
                                <th colspan="2">Force</th>
                                <th>Exemple</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="strength-very-weak">
                                <td>1</td>
                                <td>Très faible</td>
                                <td class="symbols_2"><kbd><?php
                                    echo PasswordGeneratorAnssi::generate(
                                        5,
                                        [
                                            'binary'  => true
                                        ] + $false
                                    );
                                ?></kbd></td>
                                <td class="symbols_2"><strong>5</strong> caractères parmi <strong>2</strong> caractères disponibles.</td>
                            </tr>
                            <tr class="strength-weak">
                                <td>2</td>
                                <td>Faible</td>
                                <td class="symbols_10"><kbd><?php
                                    echo PasswordGeneratorAnssi::generate(
                                        20,
                                        [
                                            'numbers'  => true
                                        ] + $false
                                    );
                                ?></kbd></td>
                                <td class="symbols_10"><strong>20</strong> caractères parmi <strong>10</strong> caractères disponibles.</td>
                            </tr>
                            <tr class="strength-medium">
                                <td>3</td>
                                <td>Moyen</td>
                                <td class="symbols_36"><kbd><?php
                                    echo PasswordGeneratorAnssi::generate(
                                        16,
                                        [
                                            'numbers'  => true,
                                            'uppercase_letters' => true
                                        ] + $false
                                    );
                                ?></kbd></td>
                                <td class="symbols_36"><strong>16</strong> caractères parmi <strong>36</strong> caractères disponibles.</td>
                            </tr>
                            <tr class="strength-strong">
                                <td>4</td>
                                <td>Fort</td>
                                <td class="symbols_26"><kbd><?php
                                    echo PasswordGeneratorAnssi::generate(
                                        22,
                                        [
                                            'uppercase_letters' => true
                                        ] + $false
                                    );
                                ?></kbd></td>
                                <td class="symbols_26"><strong>22</strong> caractères parmi <strong>26</strong> caractères disponibles.</td>
                            </tr>
                            <tr class="strength-very-strong">
                                <td>5</td>
                                <td>Très fort</td>
                                <td class="symbols_62"><kbd><?php
                                    echo PasswordGeneratorAnssi::generate(
                                        25,
                                        [
                                            'numbers'  => true,
                                            'lowercase_letters' => true,
                                            'uppercase_letters' => true
                                        ] + $false
                                    );
                                ?></kbd></td>
                                <td class="symbols_62"><strong>25</strong> caractères parmi <strong>62</strong> caractères disponibles.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="h4">Classes de caractères</div>

                <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-striped libriciel-password libriciel-password-theory">
                        <colgroup>
                            <col class="alphabet"></col>
                            <col class="symbols"></col>
                            <col class="force-1"></col>
                            <col class="force-2"></col>
                            <col class="force-3"></col>
                            <col class="force-4"></col>
                            <col class="force-5"></col>
                        </colgroup>
                        <thead>
                            <tr>
                                <th rowspan="2">Nombre de caractères</th>
                                <th rowspan="2">Caractères disponibles</th>
                                <th colspan="5">Force</th>
                            </tr>
                            <tr>
                                <th>Très faible</th>
                                <th>Faible</th>
                                <th>Moyen</th>
                                <th>Fort</th>
                                <th>Très fort</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr class="symbols_2">
                                <td>2</td>
                                <td><kbd>0</kbd> ou <kbd>1</kbd></td>
                                <td class="number strength-very-weak">&lt; 64</td>
                                <td class="number strength-weak">64</td>
                                <td class="number strength-medium">80</td>
                                <td class="number strength-strong">100</td>
                                <td class="number strength-very-strong">128</td>
                            </tr>

                            <tr class="symbols_10">
                                <td>10</td>
                                <td><kbd>0</kbd> à <kbd>9</kbd></td>
                                <td class="number strength-very-weak">&lt; 20</td>
                                <td class="number strength-weak">20</td>
                                <td class="number strength-medium">25</td>
                                <td class="number strength-strong">31</td>
                                <td class="number strength-very-strong">39</td>
                            </tr>

                            <tr class="symbols_16">
                                <td>16</td>
                                <td><kbd>0</kbd> à <kbd>9</kbd> et <kbd>A</kbd> à <kbd>F</kbd></td>
                                <td class="number strength-very-weak">&lt; 16</td>
                                <td class="number strength-weak">16</td>
                                <td class="number strength-medium">20</td>
                                <td class="number strength-strong">25</td>
                                <td class="number strength-very-strong">32</td>
                            </tr>

                            <tr class="symbols_26">
                                <td>26</td>
                                <td><kbd>A</kbd> à <kbd>Z</kbd></td>
                                <td class="number strength-very-weak">&lt; 14</td>
                                <td class="number strength-weak">14</td>
                                <td class="number strength-medium">18</td>
                                <td class="number strength-strong">22</td>
                                <td class="number strength-very-strong">28</td>
                            </tr>

                            <tr class="symbols_36">
                                <td>36</td>
                                <td><kbd>0</kbd> à <kbd>9</kbd> et <kbd>A</kbd> à <kbd>Z</kbd></td>
                                <td class="number strength-very-weak">&lt; 13</td>
                                <td class="number strength-weak">13</td>
                                <td class="number strength-medium">16</td>
                                <td class="number strength-strong">20</td>
                                <td class="number strength-very-strong">25</td>
                            </tr>

                            <tr class="symbols_52">
                                <td>52</td>
                                <td><kbd>A</kbd> à <kbd>Z</kbd> et <kbd>a</kbd> à <kbd>z</kbd></td>
                                <td class="number strength-very-weak">&lt; 12</td>
                                <td class="number strength-weak">12</td>
                                <td class="number strength-medium">15</td>
                                <td class="number strength-strong">18</td>
                                <td class="number strength-very-strong">23</td>
                            </tr>

                            <tr class="symbols_62">
                                <td>62</td>
                                <td><kbd>0</kbd> à <kbd>9</kbd>, <kbd>A</kbd> à <kbd>Z</kbd> et <kbd>a</kbd> à <kbd>z</kbd></td>
                                <td class="number strength-very-weak">&lt; 11</td>
                                <td class="number strength-weak">11</td>
                                <td class="number strength-medium">14</td>
                                <td class="number strength-strong">17</td>
                                <td class="number strength-very-strong">22</td>
                            </tr>

                            <tr class="symbols_70">
                                <td>70</td>
                                <td><kbd>0</kbd> à <kbd>9</kbd>, <kbd>A</kbd> à <kbd>Z</kbd>, <kbd>a</kbd> à <kbd>z</kbd> et
                                <?php
                                    $chars = [];
                                    foreach (PasswordGeneratorAnssi::characters(['symbols_1' => true] + $false) as $char) {
                                        $chars[] = '<kbd>'. str_replace(' ', '&nbsp;', htmlentities($char, ENT_QUOTES)).'</kbd>';
                                    }
                                    echo implode(' ', $chars);
                                ?>
                                </td>
                                <td class="number strength-very-weak">&lt; 11</td>
                                <td class="number strength-weak">11</td>
                                <td class="number strength-medium">14</td>
                                <td class="number strength-strong">17</td>
                                <td class="number strength-very-strong">21</td>
                            </tr>

                            <tr class="symbols_90">
                                <td>90</td>
                                <td><kbd>0</kbd> à <kbd>9</kbd>, <kbd>A</kbd> à <kbd>Z</kbd>, <kbd>a</kbd> à <kbd>z</kbd> et
                                <?php
                                    $chars = [];
                                    foreach (PasswordGeneratorAnssi::characters(['symbols_1' => true, 'symbols_2' => true] + $false) as $char) {
                                        $chars[] = '<kbd>'. str_replace(' ', '&nbsp;', htmlentities($char, ENT_QUOTES)).'</kbd>';
                                    }
                                    echo implode(' ', $chars);
                                ?>
                                </td>
                                <td class="number strength-very-weak">&lt; 10</td>
                                <td class="number strength-weak">10</td>
                                <td class="number strength-medium">13</td>
                                <td class="number strength-strong">16</td>
                                <td class="number strength-very-strong">20</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
               
                <p><strong>Référence:</strong> <a href="https://www.ssi.gouv.fr/administration/precautions-elementaires/calculer-la-force-dun-mot-de-passe/" target="_blank">Calculer la « force » d'un mot de passe</a> sur le <a href="https://www.ssi.gouv.fr/" target="_blank">site de l'<abbr title="Agence nationale de la sécurité des systèmes d'information">ANSSI</abbr></a></p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function affichageCouleurForceTableau(forcePassword, tableClass) {
        tableClass = typeof tableClass === 'undefined' ? 'libriciel-password-theory' : tableClass;

        $("table.libriciel-password." + tableClass + " td").removeClass('highlight');
        switch (forcePassword) {
            case 1:
                $("table.libriciel-password." + tableClass + " .strength-very-weak").addClass('highlight');
                break;

            case 2:
                $("table.libriciel-password." + tableClass + " .strength-weak").addClass('highlight');
                break;

            case 3:
                $("table.libriciel-password." + tableClass + " .strength-medium").addClass('highlight');
                break;

            case 4:
                $("table.libriciel-password." + tableClass + " .strength-strong").addClass('highlight');
                break;

            case 5:
                $("table.libriciel-password." + tableClass + " .strength-very-strong").addClass('highlight');
                break;
        }
    }

    $(document).ready(function () {
        affichageCouleurForceTableau(<?php echo $minStrength; ?>);
        affichageCouleurForceTableau(<?php echo $minStrength; ?>, 'libriciel-password-examples');


        $("table.libriciel-password.libriciel-password-examples tr td").mouseover(function () {
            var symbols = $(this).attr('class'),
                strength = $(this).closest('tr').attr('class').replace(/^.*(strength\-[^ ]+).*$/, '$1');

            $("table.libriciel-password.libriciel-password-theory td").removeClass('highlight');
            if (typeof symbols === 'undefined') {
                $("table.libriciel-password.libriciel-password-theory td." + strength).addClass('highlight');
            } else {
                $("table.libriciel-password.libriciel-password-theory tr." + symbols + " td." + strength).addClass('highlight');
            }
        });

        $("table.libriciel-password.libriciel-password-examples tr td").mouseout(function () {
            $("table.libriciel-password.libriciel-password-theory td").removeClass('highlight');
            affichageCouleurForceTableau(<?php echo $minStrength; ?>);
            affichageCouleurForceTableau(<?php echo $minStrength; ?>, 'libriciel-password-examples');
        });

    });

</script>
