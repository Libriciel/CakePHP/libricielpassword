# LibricielPassword

Plugin CakePHP 2.x (2.10.x) pour la gestion de la force des mots de passe.
Utilise _Bootstrap 3_ (3.3.5), _jQuery_ (3.2.1) et dépend de la bibliothèque
[_php-password_](https://gitlab.adullact.net/Libriciel/php-password) (1.1.0).

## Configuration

### Chargement du plugin

```php
CakePlugin::loadAll(
    [
        // ...
        'LibricielPassword' => ['bootstrap' => false, 'routes' => false],
        // ...
    ]
);
```

### Force minimale

La force minimale du mot de passe peut être configurée via la clé
`LibricielPassword.minStrength`; sa valeur par défaut est 4.

```php
Configure::write('LibricielPassword.minStrength', 4);
```

## Utilisation

### Validation de la force d'un mot de passe

Par exemple, dans la classe de modèle `User`.

```php
class User extends AppModel
{
    // ...

    public $actsAs = [
        'LibricielPassword.LibricielPassword'
    ];

    // ...

    public $validate = [
        // ...
        'passwd' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ],
            'checkPasswordStrength' => [
                'rule' => ['checkPasswordStrength']
            ],
            'compareWith' => [
                'rule' => ['compareWith', 'passwd_confirm']
            ],
        ],
        'passwd_confirm' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ],
            'compareWith' => [
                'rule' => ['compareWith', 'passwd']
            ],
        ],
        // ...
    ];
    // ...
}
```

La règle de validation `compareWith` est disponible directement via la classe
`LibricielPasswordBehavior` du plugin.

### Modale expliquant le calcul de la force des mots de passe

Cette modale contient les explications théoriques du calcul de la force d'un mot
de passe, un tableau présentant les classes de symboles utilisées ainsi qu'un
tableau d'exemples pour différentes forces de mots de passes.

#### Attention

Les symboles des classes `symbols_1` et `symbols_2` sont récupérés dynamiquement
depuis la classe `PasswordGeneratorAnssi`. ainsi, en cas de modification des
symboles utilisés par ces classes, le contenu de la modale sera toujours à jour.

Les mots de passes du tableau d'exemples sont générés à la volée, ainsi si
l'utilisateur décide d'utiliser un de ces mots de passe d'exemple, le risque de
compromission du mot de passe qu'il va utiliser est réduit, même si cette manière
de procéder n'est clairement pas à encourager (attaque [man in the middle](https://fr.wikipedia.org/wiki/Attaque_de_l%27homme_du_milieu),
accès au cache du navigateur, ...).

#### Texte et bouton faisant apparaître la modale

La modale possède l'attribut HTML `id` avec la valeur `modal-password-help`.

Le second paramètre est facultatif car il s'agit de la valeur par défaut, récupérée
depuis la configuration (clé `LibricielPassword.minStrength`).

```php
echo $this->element('LibricielPassword.password-help-block', ['minStrength' => LibricielPassword::minStrength()]);
```

#### Code de la modale

##### Inclusion de la modale depuis le code CakePHP

Le second paramètre est facultatif car il s'agit de la valeur par défaut, récupérée
depuis la configuration (clé `LibricielPassword.minStrength`).

```php
echo $this->element('LibricielPassword.Modal/password', ['minStrength' => LibricielPassword::minStrength()]);
```

A l'apparition de la modale, les cellules de la colonne correspondant à la force
minimale du mot de passe configurée obtiendront la classe CSS supplémentaire
`highlight` (voir ci-dessous).

##### Classes CSS utilisées

Lorsque l'utilisateur passe la souris au-dessus du tableau d'exemples, s'il se trouve
dans la colonne "Force", alors les cellules de la colonne de la table de théorie
correspondante, située au-dessus, obtiendront la classe CSS supplémentaire `highlight`.

Dans le cas où l'utilisateur passe la souris au-dessus de la colonne "Exemple" ou
"Description", alors seule la cellule concernée obtiendra cette classe CSS
supplémentaire.

##### Proposition de règles CSS

Du code CSS permettant de mettre en évidence la colonne de force du mot de passe
dans l'élément d'aide modal est disponible dans le fichier `webroot/css/libriciel-password.css`.
