<?php
abstract class LibricielPassword
{
    public static function minStrength()
    {
        $key = sprintf("%s.%s", __CLASS__, __FUNCTION__);
        $strength = Configure::read($key);
        if ($strength !== null && in_array($strength, [1, 2, 3, 4, 5]) === false) {
            $msgstr = "%s, invalid configured strength \"%s\" for \"%s\" key.";
            $message = sprintf($msgstr, __METHOD__, $strength, $key);
            trigger_error($message, E_USER_NOTICE);
            $strength = null;
        }
        return $strength === null ? 4 : $strength;
    }
}
